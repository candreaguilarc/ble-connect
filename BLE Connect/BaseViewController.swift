//
//  ViewController.swift
//  BLE Connect
//
//  Created by Carlo Andre Aguilar Castrat on 6/28/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreLocation
import AVFoundation

class BaseViewController: UIViewController {
    @IBOutlet weak var devicesTableView: UITableView!
    @IBOutlet weak var scanButtonContainerView: UIView!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var advertisingStatus: UILabel!
    
    @IBOutlet weak var letOthersSeeMeButton: UIButton!
    static let SERVICE_UUID = CBUUID(string: "4DF91029-B356-463E-9F48-BAB077BF3EF5")
    
    let WR_UUID = CBUUID(string: "3B66D024-2336-4F22-A980-8095F4898C42")
    let R_UUID = CBUUID(string: "4B66D024-2336-4F22-A980-8095F4898C42")
    let WR_PROPERTIES: CBCharacteristicProperties = .write
    let WR_PERMISSIONS: CBAttributePermissions = .writeable
    let systemSoundID: SystemSoundID = 1016
    let device = AVCaptureDevice.default(for: AVMediaType.video)
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    var peripheralManager: CBPeripheralManager!
    var centralManager: CBCentralManager!
    var devices: [Device] = []
    var centralManagerMessageText = ""
    var centralManagerAction = ""
    var selectedDevice: Device?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
        self.peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        self.scanButtonContainerView.isHidden = true
        self.scanButtonContainerView.alpha = 0
        activityIndicator.center = self.view.center
        self.view.addSubview(activityIndicator)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.centralManager?.stopScan()
    }
    
    @IBAction func scanButtonDidPress(_ sender: Any) {
        self.startScan()
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseOut], animations: {
            self.scanButtonContainerView.isHidden = true
            self.scanButtonContainerView.alpha = 0
        }, completion: nil)
        
    }
    
    @IBAction func discoverableDidPress(_ sender: Any) {
        self.startAdvertising()
    }
    
    func startScan() {
        self.title = "Scanning"
        self.navigationController?.view.layoutIfNeeded()
        self.devices = []
        self.devicesTableView.reloadData()
        print("Now Scanning...")
        self.centralManager.scanForPeripherals(withServices: [BaseViewController.SERVICE_UUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey : false])
        
        UIView.animate(withDuration: 0.8, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {
            UIView.setAnimationRepeatCount(30)
            self.navigationController?.navigationBar.alpha = 0.2
            self.navigationController?.view.layoutIfNeeded()
            
        }, completion: { (_) in
            self.cancelScan()
        })
    }
    
    @objc func cancelScan() {
        self.centralManager?.stopScan()
        self.title = "Devices"
        self.navigationController?.navigationBar.alpha = 1
        print("Scan Stopped")
        print("Number of Peripherals Found: \(devices.count)")
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveLinear], animations: {
            self.scanButtonContainerView.isHidden = false
            self.scanButtonContainerView.alpha = 1
        }, completion: nil)
    }
    
    func stopAdvertising(){
        self.peripheralManager.stopAdvertising()
        self.advertisingStatus.alpha = 1
        self.advertisingStatus.isHidden = true
        self.letOthersSeeMeButton.isHidden = false
        print("stopped advertising")
    }
    
    func startAdvertising() {
        print("started advertising")
      

        
        
        
//        let proximityUUID = UUID(uuidString:
//            "39ED98FF-2900-441A-802F-9C398FC199D2")
//        let major : CLBeaconMajorValue = 100
//        let minor : CLBeaconMinorValue = 1
//        let beaconID = "com.belatrix.myDeviceRegion"
//
//        let region = CLBeaconRegion(proximityUUID: proximityUUID!,
//                                    major: major, minor: minor, identifier: beaconID)
//
//        let peripheralData = region.peripheralData(withMeasuredPower: nil)
//
//        self.peripheralManager.startAdvertising(((peripheralData as NSDictionary) as! [String : Any]))
        
        self.advertisingStatus.isHidden = false
        self.letOthersSeeMeButton.isHidden = true
        
        self.peripheralManager.startAdvertising([CBAdvertisementDataServiceUUIDsKey:[BaseViewController.SERVICE_UUID], CBAdvertisementDataLocalNameKey: UIDevice.current.name])
//        self.peripheralManager.startAdvertising(["data" : "data", "string": "data"])
        
        
        UIView.animate(withDuration: 0.8, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {
            UIView.setAnimationRepeatCount(30)
            self.advertisingStatus.alpha = 0.2
            
        }, completion: { (_) in
            self.stopAdvertising()
        })
    }
    
    func addOrUpdatePeripheralList(device: Device) {
        if !self.devices.contains(where: { $0.peripheral.identifier == device.peripheral.identifier }) {
            
            self.devices.append(device)
            self.devicesTableView?.reloadData()
        }
        else if self.devices.contains(where: { $0.peripheral.identifier == device.peripheral.identifier
            && $0.name == "unknown"}) && device.name != "unknown" {
            
            for index in 0..<self.devices.count {
                
                if (self.devices[index].peripheral.identifier == device.peripheral.identifier) {
                    
                    self.devices[index].name = device.name
                    self.devicesTableView?.reloadData()
                    break
                }
            }
            
        }
    }
    
    
    func showAlertView(torchStatus: String){
        
        let alert = UIAlertController(title:  self.selectedDevice!.name, message: "Available actions", preferredStyle: .alert)
        
        if torchStatus == "on" {
                alert.addAction(UIAlertAction(title: "Deactivate Torch", style: .destructive, handler: { action in
                    self.centralManagerMessageText = "torch"
                    self.centralManager.connect( self.selectedDevice!.peripheral, options: nil)
                }))
        } else if torchStatus == "available" {
            alert.addAction(UIAlertAction(title: "Activate Torch", style: .default, handler: { action in
                self.centralManagerMessageText = "torch"
                self.centralManager.connect( self.selectedDevice!.peripheral, options: nil)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Ring", style: .default, handler: { action in
            self.centralManagerMessageText = "ring"
            self.centralManager.connect( self.selectedDevice!.peripheral, options: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            self.centralManager.cancelPeripheralConnection(self.selectedDevice!.peripheral)
        }))
        
        if self.activityIndicator.isAnimating {
            self.activityIndicator.stopAnimating()
        }
        self.present(alert, animated: true)
    }
}

extension BaseViewController: CBCentralManagerDelegate, CBPeripheralDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            print("central.state is .poweredOn")
            self.startScan()
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print(advertisementData)
        let device = Device(peripheral: peripheral, name: "unknown" )
        if let advertisementName = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
            device.name = advertisementName
        }
        self.addOrUpdatePeripheralList(device: device)
        peripheral.delegate = self
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        self.activityIndicator.startAnimating()
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        print("connected")
    }
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        if self.activityIndicator.isAnimating {
            self.activityIndicator.stopAnimating()
        }
//        self.devices.removeAll(where: {$0.peripheral == peripheral})
//        self.devicesTableView.reloadData()
        print("disconnected")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if self.activityIndicator.isAnimating {
            self.activityIndicator.stopAnimating()
        }
        self.centralManager.cancelPeripheralConnection(peripheral)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        self.centralManagerAction = "write"
        let torchStatus = (String(data: characteristic.value ?? Data(), encoding: String.Encoding.utf8)) ?? "unavialable"

        self.showAlertView(torchStatus: torchStatus)
    }
    
}

extension BaseViewController: CBPeripheralManagerDelegate {
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        var torchStatus = ""
        if self.device?.hasTorch == true {
            if self.device?.torchMode == .on {
                torchStatus = "on"
            } else {
               torchStatus = "available"
            }
        } else{
            torchStatus = "unavailable"
        }
        let data = torchStatus.data(using: .utf8)
        request.value = data
        peripheral.respond(to: request, withResult: .success)
        
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        for request in requests {
            if let value = request.value {
                let recievedActionText = String(data: value, encoding: String.Encoding.utf8) as String?
                switch recievedActionText {
                case "torch":
                    if (device?.hasTorch ?? false) {
                        do {
                            try device!.lockForConfiguration()
                            if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                                device?.torchMode = AVCaptureDevice.TorchMode.off
                            } else {
                                try device?.setTorchModeOn(level: 1.0)
                            }
                            device!.unlockForConfiguration()
                        } catch {
                            print(error)
                        }
                    }
                case "ring":
                    AudioServicesPlaySystemSound (systemSoundID)
                case .none:
                    return
                case .some(_):
                    return
                }
                
            }
            self.peripheralManager.respond(to: request, withResult: .success)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        for service in peripheral.services! {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
            let characteristic = characteristic as CBCharacteristic
            
            if self.centralManagerAction == "write" {
                if (characteristic.uuid.isEqual(WR_UUID)) {
                    let data = self.centralManagerMessageText.data(using: .utf8)
                    peripheral.writeValue(data!, for: characteristic, type: CBCharacteristicWriteType.withResponse)
                }
            } else if self.centralManagerAction == "read" {
                if (characteristic.uuid.isEqual(R_UUID)) {
                    peripheral.readValue(for: characteristic)
                }
            }
        }
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == .poweredOn {
            print("peripheral state is powered on")
            let serialService = CBMutableService(type: BaseViewController.SERVICE_UUID, primary: true)
            let writeCharacteristics = CBMutableCharacteristic(type: WR_UUID,
                                                               properties: WR_PROPERTIES, value: nil,
                                                               permissions: WR_PERMISSIONS)
            
            let readCharacteristics = CBMutableCharacteristic(
                type: R_UUID,
                properties: [CBCharacteristicProperties.notify, CBCharacteristicProperties.read],
                value: nil,
                permissions: CBAttributePermissions.readEncryptionRequired
            )
            serialService.characteristics = [writeCharacteristics, readCharacteristics]
            peripheralManager.add(serialService)
        }
    }
    
}

extension BaseViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if devices.count == 0 {
            self.devicesTableView.setEmptyMessage("No devices found")
        } else {
            self.devicesTableView.restore()
        }
        return self.devices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeripheralCell") as! PeripheralTableViewCell
        let device = self.devices[indexPath.row]
        
        cell.name.text = device.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        devicesTableView.deselectRow(at: indexPath, animated: true)
        self.selectedDevice = self.devices[indexPath.row]
        self.centralManagerAction = "read"
        self.centralManager.connect(self.devices[indexPath.row].peripheral, options: nil)
    }
    
}



