//
//  Device.swift
//  BLE Connect
//
//  Created by Carlo Andre Aguilar Castrat on 7/2/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

import Foundation
import CoreBluetooth

class Device: NSObject {
    
    var peripheral : CBPeripheral
    var name : String
    var hasTorch = false
    
    init(peripheral: CBPeripheral, name:String) {
        self.peripheral = peripheral
        self.name = name
    }
}
