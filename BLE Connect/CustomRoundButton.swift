//
//  CustomRoundButton.swift
//  BLE Connect
//
//  Created by Carlo Andre Aguilar Castrat on 7/1/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

class CustomRoundButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.height/2
    }

}
